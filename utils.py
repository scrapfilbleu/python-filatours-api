# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*

import datetime
import re

stopRE = re.compile(r'(.*), .*')
lineRE = re.compile(r'Ligne (.*) en direction de')
direRE = re.compile(r'Ligne .* en direction de (.*)') 

def split(str):
  return str.split(':')[1].strip()

def readDate(str):
  clean = split(str)
  return datetime.datetime.strptime(clean, "%d/%m/%Y")

def readTime(date, str):
  clean = split(str)
  h = int(clean.split('h')[0])
  m = int(clean.split('h')[1])
  return date.replace(date.year, date.month, date.day, h, m, 0, 0)

def readDuration(str):
  clean = split(str)
  h = 0
  m = 0
  hours = re.compile(r'([0-9]+)\s*h').search(clean)
  minutes = re.compile(r'([0-9]+)\s*min').search(clean)
  if hours:
    h = int(hours.group(1))
  if minutes:
    m = int(minutes.group(1))
  return datetime.timedelta(seconds=h * 3600 + m * 60)

def readStop(str):
  clean = split(str)
  stop = stopRE.search(clean)
  if stop:
    return stop.group(1)
  return False

def readLine(str):
  clean = split(str)
  line = lineRE.search(clean)
  if line:
    return line.group(1)
  return False

def readDirection(str):
  clean = split(str)
  line = direRE.search(clean)
  if line:
    return line.group(1)
  join = stopRE.search(clean)
  if join:
    return join.group(1)
  return False

def toJSONDate(date):
  return date.isoformat()

def toJSONTimeDelta(time):
  return time.seconds
