# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*-

from filatours import app as application

application.debug = True
application.run()
