# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*

import utils
import datetime
import re

class FilAToursStep:
  """
  Base class for modeling a schedule step
  """

  TYPE = "indication"

  def __init__(self, element, date):
    self.element = element
    self.date = date

  def populate(self):
    self.populate_times()
    self.populate_duration()
    self.populate_stops()
    self.populate_direction()
    self.populate_line()

  def populate_times(self):
    times = self.get_item(self.element, '_middle')
    self.dep = utils.readTime(self.date, times.findAll('p')[0].text)
    self.arr = utils.readTime(self.date, times.findAll('p')[1].text)
 
  def populate_duration(self):
    self.dur = self.arr - self.dep

  def populate_direction(self):
    pass

  def populate_line(self):
    pass

  def populate_ok(self):
    assert type(self.dep) == datetime.datetime
    assert type(self.arr) == datetime.datetime
    assert type(self.dur) == datetime.timedelta
    return True

  def produce_indications(self):
    pass

  def produce_step(self):
    self.step = {
      "mode": self.STEP,
      "type": self.TYPE,
      "time": {
        "start": self.dep,
        "end": self.arr,
      },
      "duration": self.dur,
      "indic": self.produce_indications()
    }

  def get_item(self, element, component):
    return element.find('div', attrs = {'class': 'jvmalinDetail_item%s' % (component)})

  def get_strong(self, match):
    return self.get_item(self.element, '_right').find('strong', text=match).find_parent('p')

  def get_class(self):
    c = None
    if self.element.name == 'div':
      s = self.get_item(self.element, '_left')
      # Searching from "Villandry - Château de Villandry" to "Tours - St Augustin"
      # and getting details for the first journey expose "Correspondance"
      if s.text.find('Rejoindre') == 0 or s.text.find('Correspondance') == 0:
        c = FilAToursWalkStep
      if s.text.find('Bus') == 0:
        c = FilAToursBusStep
      if s.text.find('Tramway') == 0:
        c = FilAToursTramStep
    if self.element.name == 'p':
      c = FilAToursWaitStep

    if c is not None:
      pop = c(self.element, self.date)
      pop.populate()
      assert pop.populate_ok()
      pop.produce_step()
      return pop

  def get_step(self):
    return self.get_class().step

class FilAToursWalkStep(FilAToursStep):
  STEP = "walk"

  def populate_stops(self):
    self.stop = utils.readStop(self.get_strong(re.compile(r'De :.*')).text)

  def populate_direction(self):
    self.direction = utils.readDirection(self.get_strong(re.compile(r'Rejoindre.*')).text)

  def produce_indications(self):
    return [
      {
        "type": "walk",
        "line": None,
        "direction": self.direction,
        "stop": self.stop
      }
    ]

class FilAToursBusTramStep(FilAToursStep):
  """
  Generic class for populating both bus and tram
  """

  def populate_stops(self):
    self.stops = {
      "dep": utils.readStop(self.get_strong(re.compile(r'De :.*')).text),
      "arr": utils.readStop(self.get_strong(re.compile(ur'à :.*', re.UNICODE)).text)
    }

  def populate_direction(self):
    self.direction = utils.readDirection(self.get_strong(re.compile(r'Prendre.*')).text)

  def populate_line(self):
    self.line = utils.readLine(self.get_strong(re.compile(r'Prendre.*')).text)

  def produce_indications(self):
    return [
      {
        "type": "mount",
        "line": self.line,
        "direction": self.direction,
        "stop": self.stops["dep"]
      },
      {
        "type": "umount",
        "line": self.line,
        "direction": self.direction,
        "stop": self.stops["arr"]
      }
    ]

class FilAToursBusStep(FilAToursBusTramStep):
  STEP = "bus"

class FilAToursTramStep(FilAToursBusTramStep):
  STEP = "tram"

class FilAToursWaitStep(FilAToursStep):
  STEP = "wait"
  TYPE = "connection"

  def populate_ok(self):
    assert type(self.dur) == datetime.timedelta
    return True

  def populate(self):
    self.dep = None
    self.arr = None
    self.dur = utils.readDuration(self.element.text)
