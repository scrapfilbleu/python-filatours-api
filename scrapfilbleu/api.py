# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*-

from flask import request
from flask.ext import restful
import requests
import datetime
import os

import replies
import scrapping

if os.environ.get('FILATOURS_HTTP_DEBUG'):
  import logging
  import httplib
  httplib.HTTPConnection.debuglevel = 1
  logging.basicConfig()
  logging.getLogger().setLevel(logging.DEBUG)
  requests_log = logging.getLogger("requests.packages.urllib3")
  requests_log.setLevel(logging.DEBUG)
  requests_log.propagate = True

class FilAToursScrapper(restful.Resource):

  BASE    = "https://www.filbleu.fr"
  SEARCH  = "horaires-et-trajets/votre-itineraire-sur-mesure?view=itineraire"
  ENLARGE = "&task=itineraire.enlarge"
  DETAILS = "&task=itineraire.newdetail&numTrajet="

  def build_cookies(self):
    params = request.args
    assert 'cookie_name' in params.keys()
    assert 'cookie_value' in params.keys()
    return { params['cookie_name']: params['cookie_value'] }

  def options(self):
    return 200

class FilAToursCookie(FilAToursScrapper):
  def get(self):
    r = requests.get(self.BASE)
    assert len(r.cookies.keys()) == 1
    name = r.cookies.keys()[0]
    return replies.CookieReply(name, r.cookies[name]).toJSON()

class FilAToursSearch(FilAToursScrapper):
  def build_payload(self):
    json_data = request.get_json()
    date_unformatted = datetime.datetime.strptime(json_data['date'], "%Y-%m-%dT%H:%M:%S")
    values = {
      "departure_city": json_data['departure']['city'],
      "departure_stop": json_data['departure']['stop'],
      "arrival_city": json_data['arrival']['city'],
      "arrival_stop": json_data['arrival']['stop'],
      "date_formatted": date_unformatted.strftime("%d/%m/%Y"),
      "hour": date_unformatted.strftime("%H"),
      "minute": date_unformatted.strftime("%M"),
      "sens": int(json_data['sens']),
      "criteria": int(json_data['criteria'])
    }
    return {
      "iform[Departure]": "%(departure_city)s - %(departure_stop)s" % values,
      "iform[Arrival]": "%(arrival_city)s - %(arrival_stop)s" % values,
      "iform[Date]": "%(date_formatted)s" % values,
      "iform[Hour]": "%(hour)s" % values,
      "iform[Minute]": "%(minute)s" % values,
      "iform[Sens]": "%(sens)d" % values,
      "iform[Criteria]": "%(criteria)d" % values
    }

  def do_post(self, cookies, data, url=None):
    if url is None:
      url = self.BASE + "/" + self.SEARCH
    r = requests.post(url, cookies=cookies, data=data)
    journeys = scrapping.ScrapJourneyList(r.text).get_reply()
    return journeys.toJSON()

  def post(self):
    cookies = self.build_cookies()
    payload = self.build_payload()
    try:
      try:
        try:
          return self.do_post(cookies, payload)
        except scrapping.FilAToursTryEnlarge as ex:
          return self.do_post(cookies, payload, self.BASE + ex.enlargeUrl)
        except scrapping.FilAToursHasStopAreas as ex:
          payload = scrapping.FilAToursStopMatcher(payload, ex.content).fill()
          return self.do_post(cookies, payload)
      except scrapping.FilAToursTryEnlarge as ex:
        return self.do_post(cookies, payload, self.BASE + ex.enlargeUrl)
    except scrapping.FilAToursError as ex:
      return ex.get_reply().toJSON()

class FilAToursDetails(FilAToursScrapper):
  def get(self):
    cookies = self.build_cookies()
    journey = request.args['journey']
    r = requests.post(self.BASE + "/" + self.SEARCH + self.DETAILS + journey, cookies=cookies)
    try:
      steps = scrapping.ScrapJourneySteps(r.text).get_reply()
      return steps.toJSON()
    except scrapping.FilAToursError as ex:
      return ex.get_reply().toJSON()
