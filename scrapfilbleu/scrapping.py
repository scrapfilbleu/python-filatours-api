# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*

from bs4 import BeautifulSoup
import re
import difflib
import datetime

import utils
import replies
import step

class FilAToursError(Exception):
  """
  Generic exception with a HTTP status code and message
  """

  def __init__(self, code, msg):
    self.statusCode  = code
    self.httpBody    = msg

  def get_reply(self):
    return replies.ErrorReply(self)

class FilAToursNoPropositions(FilAToursError):
  """
  Exception raised when no proposition was found
  """

  def __init__(self, content):
    soup = BeautifulSoup(content)
    err = soup.find('div', attrs = {'class': 'alert'})
    if err:
      msg = err.find(attrs = {'class': 'alert-heading'})
      exp = err.find('p')
      message = ""
      if msg and not exp:
        message = "%s" % (msg.text)
      if msg and exp:
        message = "%s: %s" % (msg.text, exp.text)
      FilAToursError.__init__(self, 404, message)

class FilAToursNoDetails(FilAToursError):
  """
  Exception raised when no proposition was found
  """

  def __init__(self):
    FilAToursError.__init__(self, 404, "Journey inexistent")

class FilAToursHasStopAreas(Exception):
  """
  Exception raised when no proposition was found
  but that we can find some StopArea to refine search
  """

  def __init__(self, content):
    self.content = content

class FilAToursTryEnlarge(Exception):
  """
  Exception raised when no proposition was found
  but that we can try to enlarge the search area
  """

  def __init__(self, url):
    self.enlargeUrl = url

class FilAToursStopMatcher:
  """
  Finding the best stopArea value matching a request
  """

  def __init__(self, original, content):
    self.original = original
    self.soup = BeautifulSoup(content)

  def stopArea_to_stopCity(self, stoparea):
    a = stoparea.split('|')
    # Heuristic to remove "(blabla)" when matching
    # Allows to match 'Tours - Gare de Tours' against 'Tours - gare de Tours (réseau SNCF)'
    if a[2].find('(') > 0:
      a[2] = a[2].split('(')[0]
    # Handle bad cases like Savonnières - SAVONNIERES - La Poste
    if a[2].count(' - ') == 1:
      a[2] = a[2].split(' - ')[1]
    finalStr = "%s - %s" % (a[3], a[2])
    return finalStr.split(' - ')

  def formatStopArea_to_stopCity(self, stoparea):
    a = stoparea.split('|')
    return unicode("%s - %s" % (a[3], a[2]))

  def fill(self):
    self.find_and_fill('DepJvmalin', 'Departure')
    self.find_and_fill('ArrJvmalin', 'Arrival')
    return self.original

  def find_and_fill(self, id, fieldId):
    field = 'iform[' + fieldId + ']'
    # We may have a value already good, let's reuse it
    alreadyFilled = self.soup.find('input', attrs = {'id': id})
    if alreadyFilled:
      # Re-use value. Improves behavior on bad cases:
      # Like 'Tours - gare de Tours (réseau SNCF)' => 'Saint-Etienne-de-Chigny - L'Espérance'
      alreadyValued = self.soup.find('input', attrs = { 'id': fieldId})
      self.original[field] = alreadyValued['value']
      self.original[str(alreadyFilled['name'])] = alreadyFilled['value']
      return

    # Find any other field incomplete and get a best match
    options = self.soup.find('select', attrs = {'id': id})
    if not options:
      return False

    stopOrig = self.original[field].split(' - ')
    bestSimCity = -1
    bestSimStop = -1
    bestStop = None
    for group in options.findAll('optgroup'):
      selects = group.findAll('option')
      for select in selects:
        stopArea = self.stopArea_to_stopCity(select['value'])
        simCity = difflib.SequenceMatcher(a=stopArea[0], b=stopOrig[0]).ratio()
        simStop = difflib.SequenceMatcher(a=stopArea[1], b=stopOrig[1]).ratio()
        if simCity >= bestSimCity and simStop > bestSimStop:
          bestSimCity = simCity
          bestSimStop = simStop
          bestStop = select

    # Fill the new values
    self.original[field] = self.formatStopArea_to_stopCity(bestStop['value'])
    self.original[str(bestStop.find_parent('select')['name'])] = bestStop['value']
    return

class FilAToursScrapper:
  """
  Base class for scrapping
  """

  def __init__(self, content):
    self.soup = BeautifulSoup(content)

  def get_reply(self):
    return self.reply

class ScrapJourneyList(FilAToursScrapper):

  """
  Build a list of journeys
  """

  def __init__(self, content):
    FilAToursScrapper.__init__(self, content)

    propositions = self.find_jvmalinList(self.soup)
    if not propositions:
      if self.hasSomeStopArea(self.soup):
        raise FilAToursHasStopAreas(content)
      else:
        maybeEnlarge = self.hasEnlargeURL(self.soup)
        if maybeEnlarge:
          raise FilAToursTryEnlarge(maybeEnlarge)
        else:
          raise FilAToursNoPropositions(content)

    journeys = []
    for proposition in propositions:
      journey = self.extract_proposition(proposition)
      if not journey:
        raise AttributeError
      journeys.append(journey)

    self.reply = replies.JourneysListReply(journeys)

  def hasSomeStopArea(self, soup):
    hasDep = soup.find('select', attrs = {'id': 'DepJvmalin'})
    hasArr = soup.find('select', attrs = {'id': 'ArrJvmalin'})
    return (hasDep or hasArr)

  def hasEnlargeURL(self, soup):
    sysMsg = soup.find('div', attrs = {'id': 'system-message'})
    if not sysMsg:
      return False

    alert = sysMsg.find('div', attrs = {'class': 'alert-message'})
    if not alert:
      return False

    link = alert.find('div').find('a')
    if not link:
      return False

    return link.get('href')

  def find_jvmalinList(self, soup):
    try:
      return soup.find('div', attrs = {'id': 'jvmalinList'}).findAll('table')
    except AttributeError as e:
      return False

  def extract_proposition(self, soup):
    try:
      parts = soup.findAll('p')
      assert len(parts) > 3
      date = utils.readDate(parts[2].text)
      dep = utils.readTime(date, parts[0].text)
      arr = utils.readTime(date, parts[1].text)
      # Detecting when we are starting on day X and finishing on day X+1
      if dep.time() > arr.time():
        arr = arr + datetime.timedelta(days=1)
      dur = arr - dep
      conn = int(utils.split(parts[5].text))
      m = re.compile("numTrajet=([0-9]+)").search(parts[4].find('a').get('href'))
      details = int(m.group(1))
      return {
        "date": date,
        "dep": dep,
        "arr": arr,
        "dur": dur,
        "conn": conn,
        "details": details
      }
    except AttributeError as e:
      return False

class ScrapJourneySteps(FilAToursScrapper):

  """
  Build details of a journey
  """

  def __init__(self, content):
    FilAToursScrapper.__init__(self, content)

    schedule_steps = self.find_jvmalinDetail(self.soup)
    if not schedule_steps:
      raise FilAToursNoDetails

    self.date = self.find_date(self.soup)

    steps = self.extract_steps(schedule_steps)
    if not steps:
      raise AttributeError

    self.reply = replies.JourneyStepsReply(steps)

  def find_date(self, soup):
    date = soup.find('strong', text='Date').find_parent('p').text
    return utils.readDate(date)

  def find_jvmalinDetail(self, soup):
    try:
      return soup.find('div', attrs = {'id': 'jvmalinDetail'})
    except AttributeError as e:
      return False

  def extract_steps(self, soup):
    steps = []
    for child in soup.children:
      if child is None or child.name is None:
        continue

      if not 'class' in child.attrs:
        continue

      if   (child.name == 'div' and 'jvmalinDetail_item' in child['class']) \
        or (child.name == 'p' and 'correspondance' in child['class']):
        steps.append(self.extract_step(child))

    return steps

  def extract_step(self, soup):
    return step.FilAToursStep(soup, self.date).get_step()
