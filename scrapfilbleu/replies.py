# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*-

from flask import jsonify
import utils

class ReplyFilATours:
  STATUS = 200

  def __init__(self):
    self.dic = { self.MAIN: {} }

  def add_property(self, id, content):
    self.dic[self.MAIN][id] = content

  def toJSON(self):
    try:
      resp = jsonify(self.dic)
      resp.status_code = self.STATUS
      return resp
    except RuntimeError:
      import json
      return json.dumps(self.dic)

class CookieReply(ReplyFilATours):
  MAIN = 'cookie'

  def __init__(self, name, value):
    ReplyFilATours.__init__(self)
    self.add_property('name', name)
    self.add_property('value', value)

class JourneysListReply(ReplyFilATours):
  MAIN = 'journeys'

  def __init__(self, journeys):
    ReplyFilATours.__init__(self)
    list = []
    for journey in journeys:
      list.append({
        'departure': utils.toJSONDate(journey['dep']),
        'arrival': utils.toJSONDate(journey['arr']),
        'duration': journey['dur'].seconds,
        'connections': journey['conn'],
        'details': journey['details']
      })
    self.add_property('list', list)

class JourneyStepsReply(ReplyFilATours):
  MAIN = 'journeysteps'

  def __init__(self, steps):
    ReplyFilATours.__init__(self)
    list = []
    for step in steps:
      s = step
      s['duration'] = utils.toJSONTimeDelta(step['duration'])
      if s['time']['start'] is not None:
        s['time']['start'] = utils.toJSONDate(s['time']['start'])
      if s['time']['end'] is not None:
        s['time']['end'] = utils.toJSONDate(s['time']['end'])
      list.append(s)
    self.add_property('list', list)

class ErrorReply(ReplyFilATours):
  MAIN = 'error'

  def __init__(self, exc):
    ReplyFilATours.__init__(self)
    self.STATUS = exc.statusCode
    self.add_property('message', exc.httpBody)
