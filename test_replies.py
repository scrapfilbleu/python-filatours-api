# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*-

from scrapfilbleu import replies
import datetime

cookie = replies.CookieReply('nameTEST', 'valueTEST')
print cookie.toJSON()

dep1 = datetime.datetime(2014, 8, 11, 7, 25, 0)
arr1 = datetime.datetime(2014, 8, 11, 8, 52, 0)
dep2 = datetime.datetime(2014, 8, 11, 9, 10, 0)
arr2 = datetime.datetime(2014, 8, 11, 10, 35, 0)
journeys = [
  {
    "date": datetime.datetime(2014, 8, 11, 0, 0, 0),
    "dep": dep1,
    "arr": arr1,
    "dur": arr1 - dep1,
    "conn": 2,
    "details": 1
  },
  {
    "date": datetime.datetime(2014, 8, 11, 0, 0, 0),
    "dep": dep2,
    "arr": arr2,
    "dur": arr2 - dep2,
    "conn": 1,
    "details": 0
  }
]
list = replies.JourneysListReply(journeys)
print list.toJSON()
