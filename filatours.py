# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*-

from flask import Flask
from flask.ext import restful
from flask_restful.utils.cors import crossdomain

from scrapfilbleu.api import FilAToursCookie, FilAToursSearch, FilAToursDetails

cors = crossdomain(
  origin='*',
  methods=['GET', 'POST', 'OPTIONS'],
  headers=['content-type']
)
app = Flask(__name__)
api = restful.Api(app, decorators=[cors])

api.add_resource(FilAToursCookie, '/cookie')
api.add_resource(FilAToursSearch, '/search')
api.add_resource(FilAToursDetails, '/details')
