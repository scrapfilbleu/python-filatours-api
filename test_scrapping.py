# vim: set ts=2 sw=2 et:
# -*- coding: utf8 -*

from scrapfilbleu import scrapping

import pprint

itineraire = ""
with open('testfiles/itineraire-1.html', 'r') as f:
  itineraire = "\n".join(f.readlines())

journeys = scrapping.ScrapJourneyList(itineraire)
print journeys.get_reply().toJSON()

itineraire2 = ""
with open('testfiles/itineraire-stoparea.html', 'r') as f:
  itineraire2 = "\n".join(f.readlines())

values = {
  "departure_city": "Ballan-Miré",
  "departure_stop": "La Taillerie",
  "arrival_city": "Tours",
  "arrival_stop": "Sanitas",
  "date_formatted": "12/08/2014",
  "hour": "09",
  "minute": "05",
  "sens": 1,
  "criteria": 0
}
payload = {
  "iform[Departure]": "%(departure_city)s - %(departure_stop)s" % values,
  "iform[Arrival]": "%(arrival_city)s - %(arrival_stop)s" % values,
  "iform[Date]": "%(date_formatted)s" % values,
  "iform[Hour]": "%(hour)s" % values,
  "iform[Minute]": "%(minute)s" % values,
  "iform[Sens]": "%(sens)d" % values,
  "iform[Criteria]": "%(criteria)d" % values
}
print payload
try:
  journeys2 = scrapping.ScrapJourneyList(itineraire2)
except scrapping.FilAToursHasStopAreas as e:
  print "Will search more"
  payload = scrapping.FilAToursStopMatcher(payload, e.content).fill()
  pprint.pprint(payload)

itineraire3 = ""
with open('testfiles/itineraire-stoparea2.html', 'r') as f:
  itineraire3 = "\n".join(f.readlines())

values = {
  "departure_city": "Ballan-Miré",
  "departure_stop": "La Taillerie",
  "arrival_city": "Joué lès Tours",
  "arrival_stop": "Joué Hôtel de Ville",
  "date_formatted": "12/08/2014",
  "hour": "09",
  "minute": "05",
  "sens": 1,
  "criteria": 0
}
payload = {
  "iform[Departure]": "%(departure_city)s - %(departure_stop)s" % values,
  "iform[Arrival]": "%(arrival_city)s - %(arrival_stop)s" % values,
  "iform[Date]": "%(date_formatted)s" % values,
  "iform[Hour]": "%(hour)s" % values,
  "iform[Minute]": "%(minute)s" % values,
  "iform[Sens]": "%(sens)d" % values,
  "iform[Criteria]": "%(criteria)d" % values
}
print payload
try:
  journeys3 = scrapping.ScrapJourneyList(itineraire3)
except scrapping.FilAToursHasStopAreas as e:
  print "Will search more"
  payload = scrapping.FilAToursStopMatcher(payload, e.content).fill()
  pprint.pprint(payload)

details = ""
with open('testfiles/details-1.html', 'r') as f:
  details = "\n".join(f.readlines())

steps = scrapping.ScrapJourneySteps(details)
print steps.get_reply().toJSON()
